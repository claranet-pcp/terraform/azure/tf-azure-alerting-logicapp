function GetModuleVersion(){
    Param([string]$repo)
      
    try {$latestversion=(invoke-expression "git ls-remote --tag $repo").Split("\n")[-1].Split("/")[2].trim()}
        catch {$_
        }
        return $latestversion
      }
  
      
  function ParseTerrafile() {
    Param([string]$File)
  
    $lines = ((Get-Content $File).Split("`n"))
    $modules = @()
    for ($i=0; $i -lt $lines.Length; $i=$i+3) {
      $module = @( $lines[$i], $lines[$i+1], $lines[$i+2] )
      $modules += ,$module
    }
  
    return ,$modules
  }
  
  function ReadTerrafile() {
    $terrafile = (Split-Path -parent $PSCommandPath)+"\Terrafile"
    if ( Test-Path $terrafile ) {
      $arr = ParseTerrafile -File $terrafile
      return ,$arr
    } else {
      Write-Host "[*] Terrafile does not exist"
    }
  }
  
  function DeleteCachedTerraformModules() {
    Write-Host " [*] Deleting cached terraform modules at '.\modules'" -foregroundcolor "Green"
    if (Test-Path .\modules) {
      Remove-Item -Recurse -Force .\modules
    }
  }
  
  
  $modules = ReadTerrafile
  DeleteCachedTerraformModules
  
  $local_modules_folder = ".\modules"
  New-Item -ItemType Directory -Force -Path $local_modules_folder > $null
  pushd $local_modules_folder
  
  foreach ( $module in $modules ) {
  
    $dest = $module[0].Trim().Replace(":","")
    $source = $module[1].Trim().Split(" ")[1].Replace("`"","")
    $version = $module[2].Trim().Split(" ")[1].Replace("`"","")
   
    $latestversion=getmoduleversion $source
  
    if($version -ne $latestversion){write-host -ForegroundColor Red -BackgroundColor Yellow "A new version ($latestversion) of this module is available - ($source)"}
  
    Write-host " [*] Checking out $version of $source to $local_modules_folder\$dest ... " -foregroundcolor "Green"
    Invoke-Expression "git clone -q -b $version $source $dest" 2>&1 | Out-Null
   
    
  }
  
  popd
  