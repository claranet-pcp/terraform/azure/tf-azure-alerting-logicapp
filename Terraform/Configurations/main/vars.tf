variable "customer" {
}

variable "environment" {
}

variable "configuration" {
}

variable "azure_location_short" {
}

variable "location" {
  description = "region location"
  default     = ""
}

variable "resource_group_name" {
  description = "Resource Group Name Variable"
  type        = string
  default     = ""
}

