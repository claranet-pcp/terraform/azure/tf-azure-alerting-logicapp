[Cmdletbinding()]
param (
    # The terraform command you wish to execute
    [Parameter(Mandatory=$true)]
    [ValidateSet("plan","apply","destroy-plan","destroy","unused-params")]
    [string]$Command,

    # Whether or not to clear and replace the cached terraform modules on this run
    [Parameter(Mandatory=$False)]
    [Switch]$NoModuleOverwrite
)

# Dynamic Environment param so we can autocomplete and validate
DynamicParam {
       
    # Create the collection of attributes
    $AttributeCollection = New-Object System.Collections.ObjectModel.Collection[System.Attribute];
        
    # Create and set the attributes
    $ParameterAttribute = New-Object System.Management.Automation.ParameterAttribute;
    $ParameterAttribute.Mandatory = $False;

    # Add the attributes to the attributes collection
    $AttributeCollection.Add($ParameterAttribute);
            
    # Create the dictionary 
    $RuntimeParameterDictionary = New-Object System.Management.Automation.RuntimeDefinedParameterDictionary;
                    
    # Generate and set the ValidateSet 
    $ArraySet = Get-ChildItem -Path "..\..\Environments" -Directory | Select-Object -ExpandProperty Name;
    $ValidateSetAttribute = New-Object System.Management.Automation.ValidateSetAttribute($ArraySet);

    # Add the ValidateSet to the attributes collection
    $AttributeCollection.Add($ValidateSetAttribute);
            
    # Create and add the dynamic param to the dict
    $RuntimeParameter = New-Object System.Management.Automation.RuntimeDefinedParameter("Environment", [string], $AttributeCollection);
    $RuntimeParameterDictionary.Add("Environment", $RuntimeParameter);
        
    # And return our dynamic parameter
    return $RuntimeParameterDictionary;
}

# Main()
Process {

    # Sets the Environment value in the terraform wrapper
    Set-AzureTerraformEnvironment -EnvironmentPath "..\..\Environments\$($PSBoundParameters["Environment"])";

    # Sets the Configuration value in the terraform wrapper
    Set-AzureTerraformConfiguration -ConfigurationPath $(Get-Location);

    # Sets the NoModuleOverwrite bool in the terraform wrapper
    Set-NoModuleOverwrite -NoModuleOverwrite $NoModuleOverwrite.IsPresent;

    # Switch to decide what we're executing
    switch ($command) {
        "plan"          { AzureTerraform\Invoke-AzureTerraformPlan };
        "apply"         {  AzureTerraform\Invoke-AzureTerraformApply };
        "destroy-plan"  {  AzureTerraform\Invoke-AzureTerraformPlan -Destroy };
        "destroy"       {  AzureTerraform\Invoke-AzureTerraformDestroy };
        "unused-params" {  AzureTerraform\Get-UnusedTerraformParameters };
    }

}

# Fin
End {}