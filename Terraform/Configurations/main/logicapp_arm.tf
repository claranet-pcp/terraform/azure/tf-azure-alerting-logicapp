resource "azurerm_template_deployment" "logicapp" {
  name                = "${local.resource_prefix}-logicapp"
  resource_group_name = var.resource_group_name
  template_body       = data.template_file.logicapp_template.rendered
  deployment_mode     = "Incremental"
}

data "template_file" "logicapp_template" {
  template = file("${path.module}/logicapp_rmtemplate.tmpl")
}

#terrform only crates this output on first run..
#shttps://github.com/terraform-providers/terraform-provider-azurerm/issues/578
/* output "WebhookURI" {
  value = "${azurerm_template_deployment.logicapp.outputs["WEBHOOKURI"]}"
}
 */
