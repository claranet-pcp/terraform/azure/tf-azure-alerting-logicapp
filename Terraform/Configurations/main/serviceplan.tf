resource "azurerm_app_service_plan" "app-serviceplan" {
  name                = "logicapp-plan"
  location            = var.location
  resource_group_name = var.resource_group_name

  sku {
    tier = "Shared"
    size = "D1"
  }
}

