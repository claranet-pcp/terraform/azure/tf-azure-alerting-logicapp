data "template_file" "connector_template" {
  template = file("${path.module}/connector_rmtemplate.tmpl")
}

resource "azurerm_template_deployment" "slackconnector" {
  name                = "${local.resource_prefix}-connector-slack"
  resource_group_name = var.resource_group_name
  template_body       = data.template_file.connector_template.rendered
  deployment_mode     = "Incremental"
  depends_on          = [azurerm_app_service_plan.app-serviceplan]
}

