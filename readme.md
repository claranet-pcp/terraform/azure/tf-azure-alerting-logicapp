# Logic app that posts a message to a slack channel when an alert fires

This template allows you to create a Logic app that has a webhook to be used from an Azure Alert. When the Alert is triggered, it will post a message to a slack channel that you specify. You need to have a slack account to use this template.

## Authorizing with Slack

After the template deployment has completed, there is a manual step that you must complete before the messages can be posted to the channel. You have to log in to your Slack account via the Logic apps UI in order to consent to give Logic apps access to your Slack:

1. Once the template has completed, navigate to the resource group you deployed it to.
2. Find the Logic app (represented by a rocket icon) in the resource list, and click it.
3. Select the **Edit** button in the command bar.
4. You'll now see the *Logic app designer*, and you'll see a card with **Slack** in the title, and an **Change Connection** text.
5. Click it and choose "Create New"
6. Sign in, and acknowledge that Logic apps can access your account. 
7. Click the Green checkmark at the bottom fo the **Slack**card.
8. Click the Save button in the command bar.

## Call from your Alerts

To call this whenever your Alert fires, you need to paste in the webhook URI into the alert:

1. Once the template has completed, navigate to the resource group you deployed it to.
2. Find the Logic app (represented by a rocket icon) in the resource list, and click it.
3. Select the **Edit** button in the command bar.
4. Edit the top logc app command. Copy the output called **WebHookURI**. 
5. Navigate to the alert you want to trigger the Logic app and select **Edit**.
6. Scroll to the bottom and paste in the **WebHook**. 
7. Click save.


## To Do
Send Email Alert (email addresses are already passed in the JSON) 
Pagerduty (https://www.pagerduty.com/docs/guides/azure-logic-apps-integration-guide/)



## Sample TF Alert

```
resource "azurerm_metric_alertrule" "test" {
  name                = "Server Down"
  location            = "${var.location}"
  resource_group_name = "${var.resource_group}"

  description = "No detected activity for 5 mins"

  enabled = true

  resource_id = "${azurerm_virtual_machine.bastion_host.id}"
  metric_name = "Percentage CPU"
  operator    = "LessThanOrEqual"
  threshold   = 0
  aggregation = "Average"
  period      = "PT5M"

  webhook_action {
    service_uri = "https://prod-05.westeurope.logic.azure.com:443/workflows/51422fa702b94621bcb9c3a762664e78/triggers/manual/paths/invoke?api-version=2016-10-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=IIX-ZZiMJ-gN_GoYxsVQ2Ssi0h7MxjcxYSXGKaSwXbQ"

    properties = {
      channel      = "#team-masterchief,#team-bowser"
      subscription = "CLARA-STG-CSP"
      email        = "james.millichip@claranet.uk"
    }
  }
}
```